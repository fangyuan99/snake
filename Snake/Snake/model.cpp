#include"model.h"
#include"view.h"

char g_Background[GAME_ROWS][GAME_COLS];
int Score = 0;
int MaxScore = 0;
SnakeHead Head;
SnakeFood Food;
SnakeBody Body[MAX_LENGTH];
IMAGE I_SnakeHeadUp, I_SnakeHeadDown, I_SnakeHeadLeft, I_SnakeHeadRight, I_SnakeBody, I_SnakeFood, I_Wall;
int Speed;
int Level[3] = { 5, 10, 25 };
int SpeedLevel;

void InitBackground()
{
    for (size_t nRow = 0; nRow < GAME_ROWS; nRow++)
    {
        for (size_t nCol = 0; nCol < GAME_COLS; nCol++)
        {
            if (nRow == 0 || nCol == 0 || nRow == GAME_ROWS - 1 || nCol == GAME_COLS - 1)
            {
                g_Background[nRow][nCol] = 1;
            }
            else
            {
                g_Background[nRow][nCol] = 0;

            }
        }

    }
}

void InitSnake()
{
    Head.length = 4;
    Head.y = 4;
    Head.x = 1;
    Head.direction = 4;
    Food.x = 0 - 1;
    Food.y = 0 - 1;

    for (size_t i = 0; i < 3; i++)
    {
        Body[i].y = 3 - i;
        Body[i].x = 1;
    }
}

void CombineBgSnake()
{//将蛇和背景合为一体
    g_Background[Head.x][Head.y] = 4;
    for (size_t i = 0; i < Head.length - 1; i++)
    {
        g_Background[Body[i].x][Body[i].y] = 2;
    }
    g_Background[Food.x][Food.y] = 3;
}

void SnakeMove()
{
    //判断蛇的移动方向
    if (IsCanMove() == 2)
    {
        EatFood();
        GetFood();
    }
    if (IsCanMove())
    {
        if (Head.direction == 4)
        {
            for (size_t i = Head.length - 1; i >= 1; i--)
            {
                Body[i].x = Body[i - 1].x;
                Body[i].y = Body[i - 1].y;
            }
            Body[0].x = Head.x;
            Body[0].y = Head.y++;
        }
        else if (Head.direction == 3)
        {
            for (size_t i = Head.length - 1; i >= 1; i--)
            {
                Body[i].x = Body[i - 1].x;
                Body[i].y = Body[i - 1].y;
            }
            Body[0].x = Head.x;
            Body[0].y = Head.y--;
        }
        else if (Head.direction == 2)
        {
            for (size_t i = Head.length - 1; i >= 1; i--)
            {
                Body[i].x = Body[i - 1].x;
                Body[i].y = Body[i - 1].y;
            }
            Body[0].x = Head.x++;
            Body[0].y = Head.y;
        }
        else
        {
            for (size_t i = Head.length - 1; i >= 1; i--)
            {
                Body[i].x = Body[i - 1].x;
                Body[i].y = Body[i - 1].y;
            }
            Body[0].x = Head.x--;
            Body[0].y = Head.y;
        }
    }
}

int IsCanMove()
{
    int flag = 0;
    //如果没撞墙就设置flag为1
    if ((Head.direction == 4 && g_Background[Head.x][Head.y + 1] != 1) ||
        (Head.direction == 3 && g_Background[Head.x][Head.y - 1] != 1) ||
        (Head.direction == 2 && g_Background[Head.x + 1][Head.y] != 1) ||
        (Head.direction == 1 && g_Background[Head.x - 1][Head.y] != 1))
        flag = 1;
    else flag = 0;
    for (size_t i = 0; i < Head.length - 1; i++)
    {//如果咬到自己的身体就设置flag为0
        if ((Head.direction == 4 && Head.y + 1 == Body[i].y&& Head.x == Body[i].x) ||
            (Head.direction == 3 && Head.y - 1 == Body[i].y&& Head.x == Body[i].x) ||
            (Head.direction == 2 && Head.x + 1 == Body[i].x&& Head.y == Body[i].y) ||
            (Head.direction == 1 && Head.x - 1 == Body[i].x&& Head.y == Body[i].y))
            flag = 0;
    }
    if ((Head.direction == 4 && Head.y + 1 == Food.y&& Head.x == Food.x) ||
        (Head.direction == 3 && Head.y - 1 == Food.y&& Head.x == Food.x) ||
        (Head.direction == 2 && Head.x + 1 == Food.x&& Head.y == Food.y) ||
        (Head.direction == 1 && Head.x - 1 == Food.x&& Head.y == Food.y))
        flag = 2;
    if (flag == 0 && ReStart() == 0)
        GameOver();
    else return flag;
}

void GetFood()
{
    srand((unsigned)time(NULL));
    int x, y;//用来存下一个食物的坐标
    do
    {
        x = 1 + rand() % (GAME_ROWS - 2);
        y = 1 + rand() % (GAME_COLS - 2);

    } while ((!IsCanFood(x, y)) || (Food.x == x&&Food.y == y));
    Food.x = x;
    Food.y = y;
}

void EatFood()
{
    if (IsCanMove() == 2)
    {
        mciSendString(_T("seek GOAL to start"), 0, 0, 0);
        mciSendString(_T("play GOAL"), 0, 0, 0);
        Score += 10;
        Head.length++;
        Body[Head.length - 1].x = Body[Head.length - 2].x - 1;
        Body[Head.length - 1].y = Body[Head.length - 2].y;
    }
}

void SaveMaxScore(int flag)
{
    if (flag == 0)//表示没有数据文件，则创建数据文件
    {
        FILE *File;
        File = fopen(".\\Data\\MaxScore.dat", "w");
        fprintf(File, "%d", Score);
        fclose(File);
    }
    else  if (Score > MaxScore&&Score != 0)
    {
        MaxScore = Score;
        cleardevice();
        outtextxy(0, 0, _T("恭喜你，你的分数成为历史最高!"));
        outtextxy(0, FONT_SIZE, _T("按回车退出"));
        getchar();
        FILE *File;
        File = fopen(".\\Data\\MaxScore.dat", "w");
        fprintf(File, "%d", Score);
        fclose(File);
    }
}

void ReadMaxScore(int &n)
{
    FILE *File;
    int flag = 1;//判断是否有历史最高分数据文件
    File = fopen(".\\Data\\MaxScore.dat", "r");
    if (File == NULL)
    {
        flag = 0;
        SaveMaxScore(flag);
        File = fopen(".\\Data\\MaxScore.dat", "r");
    }
    else
        fscanf(File, "%d", &n);
    fclose(File);
}

char KeyEvent()
{
    char chInput = 0;
    if (_kbhit() != 0)
    {
        //kbhit返回非0，表示键盘被按
        chInput = _getch();
    }
    return chInput;
}

int IsCanFood(int x, int y)
{
    int flag = 1;
    g_Background[Head.x][Head.y] = 4;
    for (size_t i = 0; i < Head.length - 1; i++)
    {
        g_Background[Body[i].x][Body[i].y] = 2;
    }
    if (g_Background[x][y] != 0)
        flag = 0;
    InitBackground();
    return flag;
}

void ShowInt(int x, int y, int n)
{
    TCHAR s[6];
    _stprintf(s, _T("%d"), n);		// 高版本 VC 推荐使用 _stprintf_s 函数
    outtextxy(x, y, s);
}


void LoadData()
{
    //图片
    loadimage(&I_SnakeHeadUp, _T(".\\Data\\角色上.jpg"), FONT_SIZE, FONT_SIZE);
    loadimage(&I_SnakeHeadDown, _T(".\\Data\\角色下.jpg"), FONT_SIZE, FONT_SIZE);
    loadimage(&I_SnakeHeadLeft, _T(".\\Data\\角色左.jpg"), FONT_SIZE, FONT_SIZE);
    loadimage(&I_SnakeHeadRight, _T(".\\Data\\角色右.jpg"), FONT_SIZE, FONT_SIZE);
    loadimage(&I_SnakeBody, _T(".\\Data\\身体.jpg"), FONT_SIZE, FONT_SIZE);
    loadimage(&I_SnakeFood, _T(".\\Data\\食物.jpg"), FONT_SIZE, FONT_SIZE);
    loadimage(&I_Wall, _T(".\\Data\\墙.jpg"), FONT_SIZE, FONT_SIZE);
    //背景音乐
    mciSendString(_T("open .\\Data\\背景音乐.mp3 alias BGM"), 0, 0, 0);
    mciSendString(_T("open .\\Data\\得分.mp3 alias GOAL"), 0, 0, 0);
    mciSendString(_T("open .\\Data\\失败.mp3 alias GAMEOVER"), 0, 0, 0);
    mciSendString(_T("play BGM repeat"), 0, 0, 0);
    //最高分
    ReadMaxScore(MaxScore);
}

void Music(int n)
{
    if (n == 0)
    {
        mciSendString(_T("pause BGM"), 0, 0, 0);
    }
    else
    {
        mciSendString(_T("resume BGM"), 0, 0, 0);
    }
}