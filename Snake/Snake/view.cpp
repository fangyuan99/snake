#include"view.h"
#include"model.h"

void ShowBackground()
{//1为墙，2为蛇身体，3为食物,4为蛇头,0为空
    LOGFONT f;
    SetText(f);
    outtextxy(0, 0, _T("分数:"));
    ShowInt(FONT_SIZE*3, 0, Score);
    outtextxy(FONT_SIZE*5 , 0, _T("历史最高分数:"));
    ShowInt(FONT_SIZE*12, 0, MaxScore);

    for (size_t nRow = 0; nRow < GAME_ROWS; nRow++)
    {
        for (size_t nCol = 0; nCol < GAME_COLS; nCol++)
        {
            
            switch (g_Background[nRow][nCol])
            {
            case 1:putimage( (nCol+1)*FONT_SIZE, GAP + (nRow+1)*FONT_SIZE, &I_Wall); break;
            case 2:putimage( (nCol+1)*FONT_SIZE, GAP + (nRow+1)*FONT_SIZE, &I_SnakeBody); break;
            case 3:putimage( (nCol+1)*FONT_SIZE, GAP + (nRow+1)*FONT_SIZE, &I_SnakeFood); break;
            case 4:
                if (Head.direction == 1)
                {
                    putimage((nCol + 1)*FONT_SIZE, GAP + (nRow + 1)*FONT_SIZE, &I_SnakeHeadUp); break;
                }
                else if (Head.direction == 2)
                {
                    putimage((nCol + 1)*FONT_SIZE, GAP + (nRow + 1)*FONT_SIZE, &I_SnakeHeadDown); break;
                }
                else if (Head.direction == 3)
                {
                    putimage((nCol + 1)*FONT_SIZE, GAP + (nRow + 1)*FONT_SIZE, &I_SnakeHeadLeft); break;
                }
                else
                {
                    putimage((nCol + 1)*FONT_SIZE, GAP + (nRow + 1)*FONT_SIZE, &I_SnakeHeadRight); break;
                }
                
            }
        }
    }
}

void ShowGame()
{
    
    CombineBgSnake();
    ShowBackground();
    InitBackground();
}

void OnRight(int n)
{
    if (n!=3)
    Head.direction = 4;
}

void OnLeft(int n)
{
    if (n!=4)
    Head.direction = 3;
}

void OnDown(int n)
{
    //向上不能直接转换为向下
    if (n!=1)
    Head.direction = 2;
}

void OnUp(int n)
{
    if (n!=2)
    Head.direction = 1;
}

void GameOver()
{
    SaveMaxScore(1);
    cleardevice();
    outtextxy(0, FONT_SIZE, _T("按回车键退出"));
    exit(0);
}

void Welcome()
{
    LOGFONT f;
    int nInput;
    SetText(f);
    outtextxy(FONT_SIZE*(GAME_COLS -7)/2, GAP, _T("贪吃的老八小游戏"));
    settextcolor(GREEN);
    LOGFONT f1;
    gettextstyle(&f);						// 获取当前字体设置
    f.lfHeight = FONT_SIZE*2/3;						// 设置字体高度为 48
    _tcscpy(f.lfFaceName, _T("黑体"));		// 设置字体为“黑体”(高版本 VC 推荐使用 _tcscpy_s 函数)
    f.lfQuality = ANTIALIASED_QUALITY;		// 设置输出效果为抗锯齿  
    settextstyle(&f);
    outtextxy(GAP, GAP+FONT_SIZE, _T("1.开始游戏"));
    outtextxy(GAP, GAP + FONT_SIZE * 2, _T("2.退出"));
    settextcolor(BLACK);
    outtextxy(GAP, GAP + FONT_SIZE * 3, _T("使用键盘操控老八吃到奥利给"));
    outtextxy(GAP, GAP + FONT_SIZE * 4, _T("注意不要咬到自己和墙,也不要过快的按方向键"));
    outtextxy(GAP, GAP + FONT_SIZE * 5, _T("W表示上,S表示下,A表示左,D表示右"));
    outtextxy(GAP, GAP + FONT_SIZE * 6, _T("↑表示上,↓表示下,←表示左,→表示右"));
    outtextxy(GAP, GAP + FONT_SIZE * 7, _T("按Q静音"));
    outtextxy(FONT_SIZE*(GAME_COLS - 2), FONT_SIZE*(GAME_ROWS + 1), _T("作者:方源"));
    while (1)
    {
        nInput = KeyEvent();
        if (nInput == '1')
        {
            break;
        }
        if (nInput == '2')
        {
            exit(0);
        }
    }
    SelectLevel();
}

int ReStart()
{
   // cleardevice();
    ShowGame();
    mciSendString(_T("seek GAMEOVER to start"), 0, 0, 0);
    mciSendString(_T("play GAMEOVER"), 0, 0, 0);
    outtextxy(5,5, _T("GAME OVER"));
    outtextxy(5, 5 + FONT_SIZE, _T("1.返回主界面"));
    outtextxy(5, 5 + FONT_SIZE*2, _T("2.重新开始"));
    outtextxy(5, 5 + FONT_SIZE*3, _T("3.退出"));
    while (1)
    {
        int nInput;
        nInput = KeyEvent();
        if (nInput == '1'||nInput=='2')
        {
            //initgraph(FONT_SIZE*(GAME_COLS + 2), FONT_SIZE*(GAME_ROWS + 2));
            LOGFONT f; clock_t clkStart = clock();
            clock_t clkFinish = clock();
            SaveMaxScore(Score);
            Score = 0;
            int nMusic = 1;//判断音乐是否暂停
            SetText(f);
            InitBackground();
            InitSnake();
            if (nInput == '1')
            {
                Welcome();
            }
            ShowGame();
            GetFood();
            while (1)
            {
                //进行自动移动
                clkFinish = clock();
                Music(nMusic);
                switch (KeyEvent())
                {
                case'd':
                case'D':
                case 77:
                    OnRight(Head.direction);
                    break;
                case'a':
                case'A':
                case 75:
                    OnLeft(Head.direction);
                    break;
                case'w':
                case'W':
                case 72:
                    OnUp(Head.direction);
                    break;
                case's':
                case'S':
                case 80:
                    OnDown(Head.direction);
                    break;
                case'q':
                case 'Q':
                    nMusic = (nMusic + 1) % 2;
                    break;
                default:
                    break;
                }
                Speed = 500 - Head.length*SpeedLevel;
                if (clkFinish - clkStart > ((Speed <= 100) ? 100 : Speed))
                {
                    SnakeMove();
                    ShowGame();
                    clkStart = clkFinish;
                }

            }

        }
        if (nInput == '3')
            return 0;
    }
    
}



void SetText(LOGFONT &f)
{
    gettextstyle(&f);						// 获取当前字体设置
    f.lfHeight = FONT_SIZE;						// 设置字体高度为 48
    _tcscpy(f.lfFaceName, _T("黑体"));		// 设置字体为“黑体”(高版本 VC 推荐使用 _tcscpy_s 函数)
    f.lfQuality = ANTIALIASED_QUALITY;		// 设置输出效果为抗锯齿  
    settextstyle(&f);						// 设置字体样式
    settextcolor(RED);
    setbkcolor(WHITE);
    cleardevice();
}

void SelectLevel()
{
    int nInput;
    cleardevice();
    settextcolor(GREEN);
    LOGFONT f;
    gettextstyle(&f);						// 获取当前字体设置
    f.lfHeight = FONT_SIZE * 2 / 3;						// 设置字体高度为 48
    _tcscpy(f.lfFaceName, _T("黑体"));		// 设置字体为“黑体”(高版本 VC 推荐使用 _tcscpy_s 函数)
    f.lfQuality = ANTIALIASED_QUALITY;		// 设置输出效果为抗锯齿  
    settextstyle(&f);
    outtextxy(GAP, GAP + FONT_SIZE, _T("请选择难度"));
    outtextxy(GAP, GAP + FONT_SIZE * 2, _T("1.简单模式"));
    outtextxy(GAP, GAP + FONT_SIZE * 3, _T("2.中等模式"));
    outtextxy(GAP, GAP + FONT_SIZE * 4, _T("3.困难模式"));
    while (1)
    {
        nInput = KeyEvent();
        if (nInput == '1')
        {
            SpeedLevel = Level[0]; 
            break;
        }
        else if (nInput == '2')
        {
            SpeedLevel = Level[1];
            break;
        }
        else if (nInput == '3')
        {
            SpeedLevel = Level[2];
            break;
        }
        
    }
}