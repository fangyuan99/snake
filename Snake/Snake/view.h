#pragma once
#include<graphics.h>
#include<stdio.h>
#include<Windows.h>
#include"model.h"

void ShowBackground();
void GameOver();
void ShowGame();
void OnRight(int n);
void OnUp(int n);
void OnDown(int n);
void OnLeft(int n);
void Welcome();
int  ReStart();
void SetText(LOGFONT &f);
void Show();
void SelectLevel();
