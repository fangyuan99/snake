#pragma once

#include<time.h>
#include<stdlib.h>
#include<conio.h>
#include<easyx.h>
#pragma comment(lib,"winmm.lib")

#define GAME_ROWS 12//游戏高度
#define GAME_COLS 18//游戏宽度
#define MAX_LENGTH GAME_ROWS*GAME_COLS
#define FONT_SIZE 48
#define GAP 10


 struct SnakeHead//蛇头的结构体,1为上，2为下，3为左，4为右
{
    int  direction;//方向
    int x;//蛇的坐标
    int y;
    int length;//蛇的长度
};
struct SnakeBody//蛇身体结点的结构体
{
    int x;
    int y;
};
struct SnakeFood//食物的结构体
{
    int x;
    int y;
};

extern char g_Background[GAME_ROWS][GAME_COLS];
extern SnakeHead Head;
extern SnakeBody Body[MAX_LENGTH];
extern SnakeFood Food;
extern int Score;
extern int MaxScore;
extern IMAGE I_SnakeHeadUp, I_SnakeHeadDown,I_SnakeHeadLeft,I_SnakeHeadRight,I_SnakeBody, I_SnakeFood, I_Wall;
extern int Speed;
extern int Level[3];
extern int SpeedLevel;

void InitBackground();
void InitSnake();
void SnakeMove();
void GetFood();
void EatFood();
void CombineBgSnake();
int IsCanMove();
void SaveMaxScore(int flag);
void ReadMaxScore(int &n);
char KeyEvent();
int IsCanFood(int x, int y);
void ShowInt(int x, int y, int n);
void LoadData();
void Music(int n);






