// Snake.cpp : 定义控制台应用程序的入口点。
//

#include"model.h"
#include"view.h"


int main()
{
   
    initgraph(FONT_SIZE*(GAME_COLS + 2),FONT_SIZE*(GAME_ROWS + 2) );
    LOGFONT f; clock_t clkStart = clock();
    clock_t clkFinish = clock();
    int nMusic=1;//判断音乐是否暂停
    SetText(f);
    InitBackground();
    InitSnake();
    LoadData();
    Welcome();
    ShowGame();
    GetFood();
    while (1)
    {
        //进行自动移动
        clkFinish = clock();
        Music(nMusic);
        Speed = 500 - Head.length*SpeedLevel;//蛇移动速度
            switch (KeyEvent())
            {
            case'd':
            case'D':
            case 77:
                OnRight(Head.direction);
                break;
            case'a':
            case'A':
            case 75:
                OnLeft(Head.direction);
                break;
            case'w':
            case'W':
            case 72:
                OnUp(Head.direction);
                break;
            case's':
            case'S':
            case 80:
                OnDown(Head.direction);
                break;
            case'q':
            case 'Q':
                nMusic = (nMusic + 1) % 2;
                break;
            default:
                break;
            }
        if (clkFinish - clkStart >((Speed<=100)?100:Speed))
        {
            SnakeMove();
            ShowGame();
            clkStart = clkFinish;
        }
        
    }
    system("pause");
	return 0;
}

